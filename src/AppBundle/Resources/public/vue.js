import Vue from 'vue';

import VueResource from 'vue-resource';
Vue.use(VueResource);

new Vue({
    delimiters: ['${', '}'],
    el: '#app',
    http: {
        root: '/api'
    },
    data: {
        message: 'Hello Vue right watch!'
    },
    methods: {
        fetchMaps(){
            {
                this.$http.get('/api/maps').then((response) => {
                    this.message = response.body;
                });
            }
        }
    },
    mounted: function () {
        console.log('ready');
        this.fetchMaps();
    }
});
