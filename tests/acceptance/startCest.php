<?php

class startCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->waitForElement('#app');
    }

    // tests
    public function divIdAppIsNotEmpty(AcceptanceTester $I)
    {
        $I->assertNotEquals('', $I->grabTextFrom('#app'));
    }
}
